#ifndef BODY__
#define BODY__
#include <stdio.h>
#include <iostream>
class Body{
	protected: 
		double position[2];
		double length[2];
		int type;
	public:
		Body(){
			this->position[0] = 1;
			this->position[1] = 1;
			this->length[0] = 1;
			this->length[1] = 1;
			this->type = 1;
		}
		Body(double x, double y, int type){
			this->position[0] = x;
			this->position[1] = y;
			this->length[0] = 1;
			this->length[1] = 1;
			this->type = type;
		}
		void setType(int t){
			this->type = t;
		}
		void setPosition(double x, double y){
			this->position[0]= x;
			this->position[1] = y;
		}
		void setLength(double x, double y){
			this->length[0]= x;
			this->length[1] = y;
		}
		/*
		copy constructor not working ATM
		Body &Body::operator =(const Body &x){
			this->position[0] = x.position[0];
			this->position[1] = x.position[1];
			this->type = x.type;
		}
		*/
		virtual void print(){
			printf("Direction:\t-xdir-\t-ydir-\n");
			printf("Position:\t%.2f\t%.2f\n", this->position[0], this->position[1]);
			printf("Type:\t%d\n\n", this->type);
		}
		virtual double getPosition(int dir){
			//0 means x dir
			//1 means y dir
			//all others need to fail
			if(dir<0 || dir>1){
				std::cout<<"param must be [0,1]\n";
				return -1;
			}
			return this->position[dir];
		}
		virtual double getLength(int dir){
			//0 means x dir
			//1 means y dir
			//all others need to fail
			if(dir<0 || dir>1){
				std::cout<<"param must be [0,1]\n";
				return -1;
			}
			return this->length[dir];
		}
		virtual int getType(){
			return this->type;
		}
		bool inRect(double x, double y){
			//compute box size we have top left
			//we need top right, bottom left and bottom right
			double topL = this->position[0];
			double topR = this->position[0] + this->length[0];
			double botL = this->position[1] + this->length[1];
			//if y < this boxes y, it will trigger in the other box, so we let it be in this one
			if(y< this->position[1])
				return false;
			//if the y is > this box's length + this box's y, they can't hit
			else if( y> botL)
				return false;
			//now y's are in the same range of being hit
			//if x is in the box's x and y is in the box's y, then they are colliding
			else if( x>= topL && x<topR)
				return true;
			//if the y's are in the same range, but the x's aren't, then they are not colliding
			else
				return false;
		}
/*		virtual handleCollision(Body* a){
			//do nothing, because bodies can't move
		}*/
};
/*
int main(){
	Body* a;
	a = new Body();
}
*/
#endif
