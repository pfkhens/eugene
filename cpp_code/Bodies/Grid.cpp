class Grid{
	private: 
		int ar[50][40];
	public: 
		Grid(){
			for(int i = 0; i<50; ++i){
				for(int j = 0; j<40; ++j)
					ar[i][j] = 0;
			}
		}
		void pullPos(int x, int y){
			this->ar[y][x] = 0;
		}
		void pushPos(int x, int y, int n){
			this->ar[y][x] = n;
		}
		void print(){
			for(int i = 0; i<50; ++i){
				for(int j = 0; j<40; ++j)
					printf("%d", ar[i][j]);
				printf("\n");
			}
		}
};
