#include <iostream>
#include <string>
#include "Body.cpp"
#include "Moveable.cpp"
using namespace std;

void sort(int numBodies, Body** cont){
	int i, j;
	for(i = 1; i < numBodies; ++i){
		Body* a = cont[i];
		for(j = i; j>0; --j){
			//this part will do the comparision of objects... 
			//an object is less than another if its y value is less than another... 
			//if their y values are equal... then we sort by the x value
			if(a->getPosition(1) - cont[j-1]->getPosition(1)<0){
				cont[j]= cont[j-1];
			}
			else if((a->getPosition(1) - cont[j-1]->getPosition(1))==0){
				if((a->getPosition(0) - cont[j-1]->getPosition(0))<0)
					cont[j]= cont[j-1];
				else break;
			}
			else
				break;
		}
		cont[j] = a;
	}
}
int makeCQueue(int size, Body** cont, Body** cqueue, Body* test, int location){
	int ccounter = 0, i;
	for(i = 0; i<size;++i)
		cqueue[i]=NULL;
	for(i = 0; i<size; ++i){
		if(location !=i){
			if(test->inRect(cont[i]->getPosition(0), cont[i]->getPosition(1)))
				cqueue[ccounter++] =cont[i];
		}
	}
	return ccounter;
}
void printObjects(Body** list, int listSize, string mesg ){
	//if null just print list. else print the mesg, then print the list
	if(!(mesg.compare("") ==0))
		cout<<mesg<<endl;
	for(int i = 0; i< listSize; ++i)
		list[i]->print();
}
int main(){
	//declare our variables, and push them into our movable object
	double xPos, yPos, type, yAccel, time;
	Body** container; Body** collide; Body** cqueue;
	int turns, numBodies = 0, numCollided = 0;
	//cin>> xPos>>yPos>>type>>yAccel>>time>>turns;
	string lineIdentifier;
	int lineCounter = 1;
	while(cin>>lineIdentifier){
		switch(lineCounter){
		case 1: cin>>xPos;
				//cout<<"xPos: "<<xPos<<endl;
			break;
		case 2: cin>>yPos;
				//cout<<"yPos: "<<yPos<<endl;		
			break;
		case 3: cin>>type;
				//		cout<<"type: "<<type<<endl;
			break;
		case 4: cin>>yAccel;
				//cout<<"yAccel: "<<yAccel<<endl;
			break;
		case 5: cin>>time;
				//cout<<"time: "<<time<<endl;
			break;
		case 6: cin>>turns;
				//cout<<"turns: "<<turns<<endl;
			break;
		}
			lineCounter++;
	}
	//now we can store all the objects in the same array(and any derived class as well)
	//create the container, and the collision queue
	container = new Body*[10];
	cqueue = new Body*[10];
	//create some test objects
	container[numBodies++] = new Body();
	container[numBodies++] = new Moveable(xPos, yPos--, type, yAccel);
	container[numBodies++] = new Moveable(xPos, yPos--, type, yAccel);
	container[numBodies++] = new Moveable(xPos, yPos--, type, yAccel);
	container[numBodies++] = new Moveable(xPos, yPos--, type, yAccel);
	container[numBodies++] = new Moveable(xPos, yPos--, type, yAccel);

	
	//printObjects(container, numBodies, "printing the container");
	//prove that the container exists and is sorted
	//by printing all the numbers in the container
	sort(numBodies, container);
	//printObjects(container, numBodies, "printing the sorted container");
	for(int i =0; i< numBodies; ++i){
		numCollided =makeCQueue(numBodies, container, cqueue, container[i], i);
		cout<<"number in collision queue: "<< numCollided<<endl;
		printObjects(cqueue, numCollided, "printing the collision queue");
        //cout<<"printing the object to show its not duplicated"<<endl;
        //container[i]->print();
		for(int j =0; j<numCollided; ++j){//do nothing
		}
			//container[i]->handleCollision(cqueue[j]);
	}
	//std::cout<<collide[0]->inRect(collide[1]->getPosition(0), collide[1]->getPosition(1))<<std::endl;
	return 0;
}