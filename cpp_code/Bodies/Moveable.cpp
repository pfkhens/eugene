#ifndef MOVEABLE__
#define	MOVEABLE__
#include "Body.cpp"
class Moveable: public Body{
	private:
		double acceleration[2];
		double velocity[2];
	public:
		Moveable(): Body(){
			this->acceleration[0] = 0;
			this->acceleration[1] = 0;
			this->velocity[0] = 0;
			this->velocity[1] = 0;
			this->length[0] = 1;
			this->length[1] = 1;
			this->position[0] = 1;
			this->position[1] = 1;
		}
		Moveable(double x, double y, double type) : Body(x, y, type){
			this->acceleration[0] = 0;
			this->acceleration[1]= 0;
			this->velocity[0] = 0;
			this->velocity[1] = 0;
		}
		Moveable(double x, double y, int type, double yAccel): Body( x,y,type){
			this->acceleration[0] = 0;
			this->acceleration[1]= yAccel;
			this->velocity[0] = 0;
			this->velocity[1] = 0;
		}
		Moveable(double x, double y, int type, double yAccel, double xVel, double yVel): Body( x, y, type){
			this->acceleration[0] = 0;
			this->acceleration[1]= yAccel;
			this->velocity[0] = xVel;
			this->velocity[1] = yVel;
		}
		/*
		Copy constructor is not working ATM
		Moveable Moveable::operator =(const Moveable &x){
			this->position[0] = x.position[0];
			this->position[1] = x.position[1];
			this->type = x.type;
			this->acceleration[0]= x.acceleration[0];
			this->acceleration[1]= x.acceleration[1];
			this->velocity[0] = x.velocity[0];
			this->velocity[1] = x.velocity[1];
		}
		*/
		void nPosition(int dir, double t){
			/*from the formula
			pos = .5at^2 + vnaughtt  + ynaught
			*/
			this->position[dir] = ((.5 *this->acceleration[dir]*(t*t)) + this->velocity[dir]*t + this->position[dir]); 
		}
		void nVelocity(int dir, double t){
			this->velocity[dir] = this->velocity[dir] + this->acceleration[dir]*t;
		}
		virtual void print(){
			printf("Direction:\t-xdir-\t-ydir-\n");
			printf("Position:\t%.2f\t%.2f\n", this->position[0], this->position[1]);
			printf("Type:\t%d\n", this->type);
			printf("Acceleration:\t%.2f\t%.2f\n", this->acceleration[0], this->acceleration[1]);
			printf("Velocity:\t%.2f\t%.2f\n\n", this->velocity[0], this->velocity[1]);
		}
		double getVelocity(int dir){
			return this->velocity[dir];
		}
		double getAcceleration(int dir){
			return this->acceleration[dir];
		}
		double getPosition(int dir){
			return this->position[dir];
		}
};
#endif