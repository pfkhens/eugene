# Initial README file

"Eugene" is a 2D side-scrolling adventure/puzzle game.

Although origingally this game was meant to be written in C++, the game is written entirely in Lua using the Love2D engine. There are future plans to expand the game to mobile platforms. It currently works the best on Mac OS.