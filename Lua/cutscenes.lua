
cutscenes = {}
function cutscenes.load()
	newScene('s0', {255,255,255})
--gets all the images set up
-- Test script for the scenes lib
--require "scene"

	newScene('s1', {255,255,255})
	newScene('s2', {255,255,255})
	newScene('s3', {255,255,255})
	newScene('s4', {255,255,255})
	newScene('s5', {255,255,255})
	newScene('s6', {255,255,255})
	newScene('s7', {255,255,255})
	newScene('s8', {255,255,255})
	newScene('s9', {255,255,255})
	newScene('s10', {255,255,255})
	newScene('s11', {255,255,255})
	newScene('s12', {255,255,255})
	newScene('s13', {255,255,255})
	newScene('s14', {255,255,255})
	newScene('s15', {255,255,255})
	newScene('s16', {255,255,255})
	newScene('s17', {255,255,255})
	newScene('s18', {255,255,255})

	newScene('s19', {255,255,255})
	newScene('s20', {255,255,255})
	newScene('s21', {255,255,255})

	newScene('g0', {255,255,255})
	newScene('g1', {255,255,255})
	newScene('g2', {255,255,255})
	newScene('g3', {255,255,255})
	newScene('g4', {255,255,255})

	scene1 = love.graphics.newImage("images/scenes/s1.png")
	scene2 = love.graphics.newImage("images/scenes/s2.png")
	scene3 = love.graphics.newImage("images/scenes/s3.png")
	scene4 = love.graphics.newImage("images/scenes/s4.png")
	scene5 = love.graphics.newImage("images/scenes/s5.png")
	scene6 = love.graphics.newImage("images/scenes/s6.png")
	scene7 = love.graphics.newImage("images/scenes/s7.png")
	scene8 = love.graphics.newImage("images/scenes/s8.png")
	scene9 = love.graphics.newImage("images/scenes/s9.png")
	scene10 = love.graphics.newImage("images/scenes/s10.png")
	scene11 = love.graphics.newImage("images/scenes/s11.png")
	scene12 = love.graphics.newImage("images/scenes/s12.png")
	scene13 = love.graphics.newImage("images/scenes/s13.png")
	scene14 = love.graphics.newImage("images/scenes/s14.png")
	scene15 = love.graphics.newImage("images/scenes/s15.png")
	scene16 = love.graphics.newImage("images/scenes/s16.png")
	scene17 = love.graphics.newImage("images/scenes/s17.png")
	scene18 = love.graphics.newImage("images/scenes/s18.png")

	scene19 = love.graphics.newImage("images/scenes/s19.png")
	scene20 = love.graphics.newImage("images/scenes/s20.png")
	scene21 = love.graphics.newImage("images/scenes/s21.png")
	
	g0 = love.graphics.newImage("images/scenes/g0.png")
	g1 = love.graphics.newImage("images/scenes/g1.png")
	g2 = love.graphics.newImage("images/scenes/g2.png")
	g3 = love.graphics.newImage("images/scenes/g3.png")
	g4 = love.graphics.newImage("images/scenes/g4.png")

	scenes:setUpdate('s0', function(dt, key)
		if key == 'left' then
		elseif key == 'right' then
			gamestate = "menu"
		end
	end)	

	scenes:setUpdate('s1', function(dt, key)
		if key == 'left' then
		elseif key == 'right' then
			scenes:setActive('s2')
		end
	end)

	scenes:setUpdate('s2', function(dt, key)
		if key == 'left' then
		elseif key == 'right' then
			scenes:setActive('s3')
		end
	end)

	scenes:setUpdate('s3', function(dt, key)
		if key == 'left' then
		elseif key == 'right' then
			scenes:setActive('s4')
		end
	end)

	scenes:setUpdate('s4', function(dt, key)
		if key == 'left' then
		elseif key == 'right' then
			scenes:setActive('s5')
		end
	end)

	-- next section of game
	scenes:setUpdate('s5', function(dt, key)
		if key == 'left' then
		elseif key == 'right' then
			scenes:setActive('s6')
		end
	end)

	scenes:setUpdate('s6', function(dt, key)
		if key == 'left' then
		elseif key == 'right' then
			scenes:setActive('s7')
		end
	end)

	scenes:setUpdate('s7', function(dt, key)
		if key == 'left' then
		elseif key == 'right' then
			scenes:setActive('s8')
		end
	end)

	scenes:setUpdate('s8', function(dt, key)
		if key == 'left' then
		elseif key == 'right' then
			scenes:setActive('s9')
		end
	end)

	scenes:setUpdate('s9', function(dt, key)
		if key == 'left' then
		elseif key == 'right' then
			scenes:setActive('s10')
		end
	end)

	scenes:setUpdate('s10', function(dt, key)
		if key == 'left' then
			-- do nothing
		elseif key == 'right' then
			scenes:setActive('s11')
			gamestate = "playing"
			currLevel = "tutLevel"
		end
	end)

	-- next section of game
	scenes:setUpdate('s11', function(dt, key)
		if key == 'left' then
			--do nothing
		elseif key == 'right' then
			scenes:setActive('s12')
		end
	end)

	scenes:setUpdate('s12', function(dt, key)
		if key == 'left' then
			--do nothing
		elseif key == 'right' then
			scenes:setActive('s13')
		end
	end)

	scenes:setUpdate('s13', function(dt, key)
		if key == 'left' then
			--do nothing
		elseif key == 'right' then
			scenes:setActive('s14')
		end
	end)

	scenes:setUpdate('s14', function(dt, key)
		if key == 'left' then
			--do nothing
		elseif key == 'right' then
			scenes:setActive('s15')
		end
	end)

	scenes:setUpdate('s15', function(dt, key)
		if key == 'left' then
			--do nothing
		elseif key == 'right' then
			scenes:setActive('s16')
		end
	end)

	scenes:setUpdate('s16', function(dt, key)
		if key == 'left' then
			--do nothing
		elseif key == 'right' then
			scenes:setActive('s17')
		end
	end)

	scenes:setUpdate('s17', function(dt, key)
		if key == 'left' then
			--do nothing
		elseif key == 'right' then
			scenes:setActive('s18')
		end
	end)

	scenes:setUpdate('s18', function(dt, key)
		if key == 'left' then
			--do nothing
		elseif key == 'right' then
			wave = 0
			gamestate = "playing"
		end
	end)

	scenes:setUpdate('s19', function(dt, key)
		if key == 'left' then
			--do nothing
		elseif key == 'right' then
			scenes:setActive('s20')
		end
	end)

	scenes:setUpdate('s20', function(dt, key)
		if key == 'left' then
			--do nothing
		elseif key == 'right' then
            currLevel = "midBossLevel"
			gamestate = "playing"
		end
	end)

	scenes:setUpdate('s21', function(dt, key)
		if key == 'left' then
			--do nothing
		elseif key == 'right' then
            currLevel = "finalBossLevel"
			gamestate = "playing"
		end
	end)

	scenes:setUpdate('g0', function(dt, key)
		if key == 'left' then
			--do nothing
		elseif key == 'right' then
			scenes:setActive('g1')
		end
	end)

	scenes:setUpdate('g1', function(dt, key)
		if key == 'left' then
			--do nothing
		elseif key == 'right' then
			scenes:setActive('g2')
		end
	end)

	scenes:setUpdate('g2', function(dt, key)
		if key == 'left' then
			--do nothing
		elseif key == 'right' then
			scenes:setActive('g3')
		end
	end)

	scenes:setUpdate('g3', function(dt, key)
		if key == 'left' then
			--do nothing
		elseif key == 'right' then
			scenes:setActive('g4')
		end
	end)

	scenes:setUpdate('g4', function(dt, key)
		if key == 'left' then
			--do nothing
		elseif key == 'right' then
			wave = 0
			gameFinishBGM:stop()
			gamestate = "menu"
			scenes:setActive('s1')
		end
	end)


	scenes:setDraw('s0', function()
		love.graphics.setColor(255,255,255)
	    love.graphics.push()
        love.graphics.setColor(0,255,255)
        love.graphics.print("GAME OVER", 380, 170)
        love.graphics.print("Let's try again!", 320, 300)
	    love.graphics.pop()
	end)

	scenes:setDraw('s1', function()
		love.graphics.setColor(255,255,255)
		love.graphics.draw(scene1)
	end)

	scenes:setDraw('s2', function()
		love.graphics.setColor(255,255,255)
		love.graphics.draw(scene2)
	end)

	scenes:setDraw('s3', function()
		love.graphics.setColor(255,255,255)
		love.graphics.draw(scene3)
	end)

	scenes:setDraw('s4', function()
		love.graphics.setColor(255,255,255)
		love.graphics.draw(scene4)
	end)

	scenes:setDraw('s5', function()
		love.graphics.setColor(255,255,255)
		love.graphics.draw(scene5)
	end)

	scenes:setDraw('s6', function()
		love.graphics.setColor(255,255,255)
		love.graphics.draw(scene6)
	end)

	scenes:setDraw('s7', function()
		love.graphics.setColor(255,255,255)
		love.graphics.draw(scene7)
	end)

	scenes:setDraw('s8', function()
		love.graphics.setColor(255,255,255)
		love.graphics.setBackgroundColor(0,0,0)
		love.graphics.draw(scene8)
	end)

	scenes:setDraw('s9', function()
		love.graphics.setColor(255,255,255)
		love.graphics.setBackgroundColor(0,0,0)
		love.graphics.draw(scene9)
	end)

	scenes:setDraw('s10', function()
		love.graphics.setColor(255,255,255)
		love.graphics.setBackgroundColor(0,0,0)
		love.graphics.draw(scene10)
	end)

	scenes:setDraw('s11', function()
		love.graphics.setColor(255,255,255)
		love.graphics.draw(scene11)
	end)

	scenes:setDraw('s12', function()
		love.graphics.setColor(255,255,255)
		love.graphics.setBackgroundColor(141,60,254)
		love.graphics.draw(scene12)
	end)

	scenes:setDraw('s13', function()
		love.graphics.setColor(255,255,255)
		love.graphics.draw(scene13)
	end)

	scenes:setDraw('s14', function()
		love.graphics.setColor(255,255,255)
		love.graphics.draw(scene14)
	end)

	scenes:setDraw('s15', function()
		love.graphics.setColor(255,255,255)
		love.graphics.setBackgroundColor(0,0,0)
		love.graphics.draw(scene15)
	end)

	scenes:setDraw('s16', function()
		love.graphics.setColor(255,255,255)
		love.graphics.setBackgroundColor(0,0,0)
		love.graphics.draw(scene16)
	end)

	scenes:setDraw('s17', function()
		love.graphics.setColor(255,255,255)
		love.graphics.setBackgroundColor(0,0,0)
		love.graphics.draw(scene17)
	end)

	scenes:setDraw('s18', function()
		love.graphics.setColor(255,255,255)
		love.graphics.setBackgroundColor(0,0,0)
		love.graphics.draw(scene18)
	end)

	scenes:setDraw('s19', function()
		love.graphics.setColor(255,255,255)
		love.graphics.setBackgroundColor(0,0,0)
		love.graphics.draw(scene19)
	end)

	scenes:setDraw('s20', function()
		love.graphics.setColor(255,255,255)
		love.graphics.setBackgroundColor(0,0,0)
		love.graphics.draw(scene20)
	end)

	scenes:setDraw('s21', function()
		love.graphics.setColor(255,255,255)
		love.graphics.setBackgroundColor(0,0,0)
		love.graphics.draw(scene21)
	end)

	scenes:setDraw('g0', function()
		love.graphics.setColor(255,255,255)
		love.graphics.setBackgroundColor(0,0,0)
		love.graphics.draw(g0)
	end)

	scenes:setDraw('g1', function()
		love.graphics.setColor(255,255,255)
		love.graphics.setBackgroundColor(0,0,0)
		love.graphics.draw(g1)
	end)

	scenes:setDraw('g2', function()
		love.graphics.setColor(255,255,255)
		love.graphics.setBackgroundColor(0,0,0)
		love.graphics.draw(g2)
	end)

	scenes:setDraw('g3', function()
		love.graphics.setColor(255,255,255)
		love.graphics.setBackgroundColor(0,0,0)
		love.graphics.draw(g3)
	end)

	scenes:setDraw('g4', function()
		love.graphics.setColor(255,255,255)
		love.graphics.setBackgroundColor(0,0,0)
		love.graphics.draw(g4)
	end)

	scenes:setActive('s1')
end

function cutscenes.keypressed(key)
	scenes:update(dt,key)
end

function cutscenes.update(dt, key)
	scenes:update(dt,key)
end

function cutscenes.reset()
	scenes:setActive('s1')
end

function cutscenes.draw()
	scenes:draw()
end