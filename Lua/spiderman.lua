
spiderman = {}

function spiderman.load()
    spidermanEL = love.graphics.newImage("images/spiderman-l.png")
    spidermanER = love.graphics.newImage("images/spiderman-r.png")

    spiderman.pic = spidermanEL
    spiderman.speed = 50
    spiderman.x = 500
    spiderman.y = -100
    spiderman.health = 50

    spiderman.width = spiderman.pic:getWidth()
    spiderman.height = spiderman.pic:getHeight()
end

function spiderman.move(dt)
    dx = player.x - spiderman.x
    dy = player.y - spiderman.y
    spiderman.speed = spiderman.speed
    distance = math.sqrt(dx*dx + dy*dy)
    spiderman.x = spiderman.x + (dx / distance * spiderman.speed * dt)
    spiderman.y = spiderman.y + (dy / distance * spiderman.speed * dt)

    if player.x > spiderman.x + spiderman.width then
        spiderman.pic = spidermanER
    end

    if player.x < spiderman.x - spiderman.width then
        spiderman.pic = spidermanEL
    end
end

function spiderman.update()
    if player.pic == playerER then
        spiderman.pic = spidermanEL
    end

    if player.pic == playerEL then
        spiderman.pic = spidermanER
    end

    if player.x + player.width > spiderman.x and
        player.x < spiderman.x + spiderman.width/1.5 and
        player.y + player.height > spiderman.y and
        player.y < spiderman.y + spiderman.height then
        if spiderman.direction  == 'right' then
            spiderman.x = spiderman.x + 300
        else
            spiderman.x = spiderman.x - 300
        end

        player.decrementHeath()
        player.decrementHeath()
        player.decrementHeath()
        player.decrementHeath()
        player.decrementHeath()
        player.decrementHeath()
        
    end
end

function spiderman.death()
    -- checks all of the spidermans and all of the bullets to 
    -- see which collide, then remove all the bullets and spidermans
    -- that have collided. Runtime O(n^2)
    for ia,va in ipairs(bullet) do
        if va.x + va.width > spiderman.x and
            --check to see if it intersects here
            va.x < spiderman.x + spiderman.width/2 and
            va.y + va.height > spiderman.y and
            va.y < spiderman.y + spiderman.height then
            table.remove(bullet, ia)
                spiderman.health = spiderman.health - 1
            break
        end
    end
end

function spiderman.reset()
    for i,v in ipairs(spiderman) do
        table.remove(spiderman, i)
    end
    spiderman.x = 500
    spiderman.y = -100
    spiderman.health = 50
end

function spiderman.draw()
    love.graphics.setColor(255,255,255)
    if spiderman.health > 0 then
        love.graphics.draw(spiderman.pic, spiderman.x, spiderman.y)
    end
end