camera = {}
camera.x = 0
camera.y = 0
camera.scaleX = 1
camera.scaleY = 1
camera.rotation = 0

function camera:set()
    -- puts the camera on top of eugene
    love.graphics.push()
    love.graphics.rotate(-self.rotation)
    love.graphics.scale(1/self.scaleX, 1/self.scaleY)
    love.graphics.translate(-self.x, -self.y)
end

function camera:unset()
    --take the focus off of eugene
    love.graphics.pop()
end

function camera:move(dx, dy)
    --move the camera by a differential of the directions provided
    self.x = self.x + (dx or 0)
    self.y = self.y + (dy or 0)
end

function camera:rotate(dr)
    --rotates the camera a dTheta
    self.rotation = self.rotation + dr
end

function camera:scale(sx, sy)
    --scale the cameras zoom
    sx = sx or 1
    self.scaleX = self.scaleX * sx
    self.scaleY = self.scaleY * (sy or sx)
end

function camera:setPosition(x, y)
    --similar to camera:zoom, except this takes in an absolute x, y position
    self.x = x or self.x
    self.y = y or self.y
end

function camera:getX()
    --gets the camera's x position
    --used implicitly inmove
    return self.x
end

function camera:getY()
    --gets the camera's y position
    --used implicitly in move
    return self.y
end

function camera_follow()
    --the method that has the camera follows the player
    if player.x > love.graphics.getWidth() / 2 then
        camera.x = player.x - love.graphics.getWidth() /2
    end
    if player.y > love.graphics.getWidth() / 2 then
        camera.y = player.y - love.graphics.getWidth() /2
    end    
end

function camera_reset()
    --puts the camera's position to be zero (where we begin)
    camera.x = 0
    camera.y = 0
end