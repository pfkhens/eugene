
zombie = {}

zombieEL = love.graphics.newImage("images/zombie-left.png")
zombieER = love.graphics.newImage("images/zombie-right.png")

-- Initial picture
zombie.pic = zombieEL   
zombie.speed = 250

function zombie.load()
    speed = 50
    zombie.speed = 50

    zombie.width = zombie.pic:getWidth()
    zombie.height = zombie.pic:getHeight()
end

function zombie:spawn(x,y,direction)
    --inserts a zombie into the table that holds all zombies
    --param: the x and y position that should be inserted
    --and the direction it should be drawn
    table.insert(zombie, {x = x, y = y, direction = direction, speed = 250, width = zombie.pic:getWidth(), height = zombie.pic:getHeight(), health = 2})
    --(still laggy)
end

function zombie:draw()
    for i,v in ipairs(zombie) do
        love.graphics.setColor(255, 255, 255)
        --if a zombie is moving left, make it draw the left picture
        --else draw the "right" picture  
        if v.direction == 'right' then
            love.graphics.draw(zombie.pic, v.x, v.y)
        end

        if v.direction == 'left' then
            love.graphics.draw(zombie.pic, v.x, v.y)
            
        end
    end
end
function zombie:move(dt)
    --update all the positions for
    --all of the zombies in the table
    --param: dt (the time differential since the last draw) 
    for i,v in ipairs(zombie) do  
        dx = player.x - v.x
        v.speed = zombie.speed
        distance = math.sqrt(dx*dx)
        v.x = v.x + (dx / distance * v.speed * dt)     

        if player.x > v.x + v.width then
            zombie.pic = zombieER
        end

        if player.x < v.x - v.width then
            zombie.pic = zombieEL
        end
    end
end

local spawntimer = 0
local spawntimlim = math.random(3,5)

function zombie:horde(dt)
    --this randomizes the creation of zombies
    --param: dt (time differential between draws)
    spawntimer = spawntimer + dt
    if spawntimer > spawntimlim then
        zombie:spawn(player.x - 1000, 200,'left')
        zombie:spawn(player.x + 1000, 200,'right')

--optional spawn points
--      zombie:spawn(player.x + 500, -(player.y)+150,'right')
--        zombie:spawn(player.x + 500, player.y,'right')
        -- zombie:spawn(camera.x + screenWidth, 200,'right')
        
        -- zombie:spawn(camera.x - screenWidth, 200,'left')
        math.randomseed(os.time())
        math.random()
        math.random()
        math.random()
        math.random()
        spawntimer = 0
        spawntimlim = math.random(3,5)
        
    end
end



function zombie:updateLevel(wave)
    -- this does dynamic increasing of 
    -- the zombies speed, as we increase waves
    -- this will make the game progressively harder as the game goes on 
    for i,v in ipairs(zombie) do  
        if wave == 2 then
            v.speed = v.speed * 2
        end    

        if wave == 3 then 
            v.speed = v.speed * 2
         end   
    end
end


function zombie:death()
    -- checks all of the zombies and all of the bullets to 
    -- see which collide, then remove all the bullets and zombies
    -- that have collided. Runtime O(n^2)
    for ia,va in ipairs(bullet) do
        --for all of the bullets
        for i,v in ipairs(zombie) do
            --for all the zombies
            if va.x + va.width > v.x and
                --check to see if it intersects here
                va.x < v.x + v.width and
                va.y + va.height > v.y and
                va.y < v.y + v.height then
                table.remove(bullet, ia)

                    if wave >=3 then
                        v.health = v.health - .5
                    elseif wave < 3 then 
                        v.health = v.health - 1
                    end

                    if v.health == 0 then
                        table.remove(zombie, i)
                        kills = kills + 1
                        score = kills * 1850
                    end
                break
            end
        end
    end
end

function zombie:reset()
    --removes all of the zombies in the table
    for i,v in ipairs(zombie) do
        --reference this line if you want to remove an item from a table
        if kills == 30 then
        table.remove(zombie, i)
    end
    end
    wave = 1
end