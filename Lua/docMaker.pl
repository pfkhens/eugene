#!/usr/bin/perl -w
#declare the array of files to be examined
sub catAllFiles;
sub docAllFiles;
#main begins here
@files = ();

#fill it with all the lua files in our directory
@files = `ls *.lua`;

#for each file, we need to parse it.
#this is composed of function all the functions, and prinTing out the lines immediately following 
#the function declaration (this will form the primary definition of the function)
#followed by additional comments in the function
	
catAllFiles(@files);
docAllFiles(@files);
sub catAllFiles(){
	open(CAT, ">allCode.txt");
	my @files = @_;
	foreach $x(@files){
		open (IN , "<$x");
		# #remove the new line feed from the file names
		# chomp $x;
		#find the first function in the file
		print CAT "$x";
		while($line =<IN>){
			print CAT $line;
		}
		close (IN);
	}
	close (CAT);
}
sub docAllFiles(){
	open (DOCS, ">allDocs.txt");
	$i = 1;
	my (@files) = @_;
	print DOCS "Total Number of Lua Files: ".scalar(@files)."\n";
	@tableOfContents = ();	
	foreach $x(@files){
		open (IN , "<$x");
		print DOCS "File:\n$x\n\n\n";
		#find the first function in the file
		#do the things
		#descCommentBool represents the need to differentiate between descriptive comments, and 
		#usage Comments (descriptive lay just beneath the function declaration, where
		#usage comments are a little more different between the code
		$descCommentBool = 0;
		while($line =<IN>){
			if ($line =~ "function") {
					print DOCS "\n\n$i..Function:\n\tDefinition:$line\n";
					push(@tableOfContents, $line);
					$descCommentBool = 1;
					print DOCS "\t\tDescription\n";
					$i++;
			}

			#already print DOCSed header, if descCommentBool, we need to check to see if we need to 
			#swap to inline Comments
			#split the line on spaces

			elsif ( $descCommentBool==1){
				#we are forced to splite this both here and in the else due to syntax
				for($line) { s/^\s+//; s/\s+$//;s/\s+/ /g;}
				# @lineContents = split(/ /, $line);

				#check to see if the first part indicates if its a comment
				#if it does, print DOCS that line, else swap to inline comments mode
				if($line =~ "--"){
					print DOCS "\t\t\t$line\n";
				}
				else{
					$descCommentBool = 0;
					print DOCS "\t\tInline Comments:\n\n";
				}
			}
			#if the line doesn't contain the word function, and we are past the 
			#descriptive comments then we need to print DOCS the line if it is a comment
			else{
	
				#we are forced to splite this both here and in the elsif due to perl's syntax
				for($line) { s/^\s+//; s/\s+$//;s/\s+/ /g;}
				# @lineContents = split(/ /, $line);
				if ($line =~ "--")	 {
					print DOCS "\t\t\t$line\n";
				}
			}
		}
		close (IN);
	}
	$counter = 1;
	open(TOC, ">FunctionTableOfContents.txt");
	print TOC "Table of Contents\n\n";
	foreach $y(@tableOfContents){
		print TOC "$counter $y\n";
		$counter++;
	}
	close (DOCS);
} 