require "fonts"
require "camera"
require "menu"
require "cutscenes"
require "player"
require "zombie"
require "bullet"
require "map"
require "camera"
require "splash"
require "levels/tutLevel"
require "levels/level1"
require "levels/bossLevel"
require "libraries/scene"
require "spiderman"
require "yondaime"
require "badbullet"

function love.load()
    -- Load the game
    -- Getters for screen width and height
    screenWidth = love.graphics.getWidth()
    screenHeight = love.graphics.getHeight()

    --Loading Classes
    fonts.load()
    splash.load()
    menu.load()
    cutscenes.load()

    --Load characters
    player.load()
    spiderman.load()
    yondaime.load()

    tutLevel_load()
    level1_load()
    bossLevel_load()

    -- Initializations
    -- Initialize our number of kills
    kills = 0          

    -- Counter to determine if the game will start at the cutscenes or at the first level
    playerDeath = 0     
    
    -- Initialize our wave counter
    wave = 1
    score = 0
    highScore = 0

    -- Loads our file for the high score
    filename = love.filesystem.getWorkingDirectory() .. '/highscore.txt'

    -- Sets and loads our high score
    highscore.set(filename, 5, 'name', 0)
    highscore.load()

    -- sets to splash screen
    gamestate = "splash"
    currLevel = "tutLevel"

    midBossLevelBGM = love.audio.newSource("music/midBoss.ogg")
    midBossLevelBGM:setLooping(true)

    finalBossLevelBGM = love.audio.newSource("music/finalBoss.ogg")
    finalBossLevelBGM:setLooping(true)

    gameFinishBGM = love.audio.newSource("music/gameFinish.ogg")
    gameFinishBGM:setLooping(true)

    iconimg = love.graphics.newImage("images/icon.png")
	love.graphics.setIcon(iconimg)

end

function love.update(dt, key)
    -- updates the gamestate
    if gamestate == "menu" then
         table.remove(zombie, i)
         table.remove(bullet, i)
    end    
    if gamestate == "splash" then
        splash.update()
    end

    if gamestate == "cutscenes" then
        cutscenes.update(dt, key)
    end

    if gamestate == "playing" and currLevel == "tutLevel" then
        --update Eugene's position
        updateEugene(dt)    
        --update bullet
        bullet.update(dt)
        map_collide()
        camera_follow()

        if player.x > 950 then
            player.reset()
            camera_reset()
            currLevel = "level1"
            gamestate = "cutscenes"
        end
    end

    if gamestate == "playing" and currLevel == "level1" then

        updateEugene(dt)    
        bullet.update(dt)   
        map_collide()
        camera_follow()

        zombie:horde(dt)
        zombie:move(dt)

        player:collide()
        zombie:death()

        if kills == 10 then
            wave = 2
        end

        if kills == 20 then
            wave = 3
        end

        if kills == 30 then
            scenes:setActive('s19')
            bullet.reset()
            camera_reset()
            gamestate = "cutscenes"
            levelOneBGM:stop()
            midBossLevelBGM:play()
        end

        --If we die, then we reset everything and stop the music
        if player.healthCtr > 6 then
            scenes:setActive('s0')
            love.audio.stop(levelOneBGM)
            gamestate = "cutscenes"
            player.reset()
            zombie:reset()
            bullet.reset()

            camera_reset()

            if score >= highScore then
                highScore = score
            end

            highscore.add(name, score)
            playerDeath = playerDeath + 1

            score = 0
        end

        -- we want the game to have a decent level of scaling of difficulty, 
        -- thus the offset of each wave number is weird
        if wave == 2 then
              zombie:horde(dt)
              zombie:move(dt)            
              zombie:updateLevel(wave)
        end

        if wave == 3 then
              zombie:horde(dt)
              zombie:move(dt)
              zombie:updateLevel(wave)
        end

        if wave == 7 then
              zombie:horde(dt)
              zombie:move(dt)
              zombie:updateLevel(wave)
        end
        
        if wave == 11 then
              zombie:horde(dt)
              zombie:move(dt)
              zombie:updateLevel(wave)
        end
        
        if wave == 13 then
              zombie:horde(dt)
              zombie:move(dt)
              zombie:updateLevel(wave)
        end

        if wave == 20 then
              zombie:horde(dt)
              zombie:move(dt)
              zombie:updateLevel(wave)
        end

        if wave == 26 then
              zombie:horde(dt)
              zombie:move(dt)
              zombie:updateLevel(wave)
        end
        
        if wave == 30 then
              zombie:horde(dt)
              zombie:move(dt)
              zombie:updateLevel(wave)
        end
        
        if wave == 40 then
              zombie:horde(dt)
              zombie:move(dt)
              zombie:updateLevel(wave)
        end
    end

    if gamestate == "playing" and currLevel == "midBossLevel" then

        --update Eugene's position
        updateEugene(dt)

        --update bullet
        bullet.update(dt)
        map_collide()
        camera_follow()

        player:collide()

        if spiderman.health > 0 then
            spiderman.move(dt)
            spiderman.update()
            spiderman.death()
        end

        if spiderman.health == 0 then
            scenes:setActive('s21')
            bullet.reset()
            camera_reset()
            score = score + 100000

            midBossLevelBGM:stop()
            finalBossLevelBGM:play()

            gamestate = "cutscenes"
        end

        --If we die, then we reset everything and stop the music
        if player.healthCtr > 6 then
            scenes:setActive('s0')
            gamestate = "cutscenes"
            player.reset()
            
            zombie:reset()            
            spiderman.reset()
            bullet.reset()
            camera_reset()

            if score >= highScore then
                highScore = score
            end

            highscore.add(name, score)
            playerDeath = playerDeath + 1

            score = 0
            midBossLevelBGM:stop()
        end
    end

    if gamestate == "playing" and currLevel == "finalBossLevel" then

        updateEugene(dt)    
        bullet.update(dt)   
        map_collide()
        camera_follow()

        player:collide()

        if yondaime.health > 0 then
            yondaime.move(dt)
            yondaime.update()
            yondaime.death()
            badbullet.update(dt) 
            badbullet.shoot(dt)
        end

        if yondaime.health == 0 then
            scenes:setActive('g0')
            bullet.reset()
            camera_reset()

            player.reset()
            zombie:reset()
            spiderman.reset()
            yondaime.reset()
            bullet.reset()

            score = score + 500000

            if score >= highScore then
                highScore = score
            end

            highscore.add(name, score)

            score = 0
            gamestate = "cutscenes"
            finalBossLevelBGM:stop()
            gameFinishBGM:play()
        end

        --If we die, then we reset everything and stop the music
        if player.healthCtr > 6 then
            scenes:setActive('s0')
            gamestate = "cutscenes"
            player.reset()

            zombie:reset()
            spiderman.reset()
            yondaime.reset()
            bullet.reset()

            camera_reset()

            if score >= highScore then
                highScore = score
            end

            highscore.add(name, score)
            playerDeath = playerDeath + 1

            score = 0
            finalBossLevelBGM:stop()
        end
    end
end

function love.keypressed(key)
    -- Game action handlers -- 
    if gamestate == "splash" then
        gamestate = "menu"
    end

    if gamestate == "menu" then
        menu.move(key)
    end

    if gamestate == "menu" then
        if key == 'return' and menuState == "start" and playerDeath == 0 then
            -- Jumps to the cut scenes if we start a fresh game
            gamestate = "cutscenes"     
        elseif key == 'return' and menuState == "start" and playerDeath > 0 then
            -- Jumps back to thefirst level if we die
            gamestate = "playing"       
            currLevel = "level1"
        elseif key == 'return' and menuState == "quit" then
            love.event.quit()
        end
    end

    if gamestate == "cutscenes" then
        cutscenes.keypressed(key)
    end

    if gamestate == "playing" and key == 'p' then
        gamestate = "pause" 
    end

    if gamestate == "pause" then
        pause.move(key)
    end

    if gamestate == "pause" then
        if key == 'return' and pauseState == "resume" then
            gamestate = "playing"
        elseif key == 'return' and pauseState == "quit1" then
            zombie:reset()
            spiderman.reset()
            yondaime.reset()
            score = 0
            kills = 0
            if currLevel == "level1" then
                levelOneBGM:stop()
            elseif currLevel == "midBossLevel" then
                midBossLevelBGM:stop()
            elseif currLevel == "finalBossLevel" then
                finalBossLevelBGM:stop()
            end
            gamestate = "menu"
        end
    end

    bullet.shoot(key)
end

function love.keyreleased(key)
    --show when the key is released
    player.animreset(key)   -- reset the animation
end

function love.draw()
    -- regardless of gamestate, this will draw the game
    -- draws our splash font
    if gamestate == "splash" then
        love.graphics.setBackgroundColor(0,0,0,0)
        splash.draw()
    end
    --should maybe changes these to be else ifs?
    if gamestate == "menu" then
        menu.draw()
--        menuBGM:play()
    end

    if gamestate == "cutscenes" then
        cutscenes.draw()
    end

    if gamestate == "pause" then
        pause.draw()
    end

    if gamestate == "playing" and currLevel == "tutLevel" then
        -- Pushes camera onto stack
        camera:set()
        tutLevel_draw()
        -- Draws Eugene + hearts
        drawEugene()                
        bullet.draw()

        -- Pops camera from stack
        camera:unset()
    end

    if gamestate == "playing" and currLevel == "level1" then
        camera:set()                
--        menuBGM:stop()

        level1_draw()               
        drawEugene()
        zombie.draw()
        bullet.draw()
        camera:unset()              
        levelOneBGM:play()
    end

    if gamestate == "playing" and currLevel == "midBossLevel" then
        camera:set()

        bossLevel_draw()
        drawEugene()
        spiderman.draw()
        bullet.draw()
        camera:unset()
    end

    if gamestate == "playing" and currLevel == "finalBossLevel" then
        camera:set()
        bossLevel_draw()
        drawEugene()
        badbullet.draw()
        yondaime.draw()
        bullet.draw()
        camera:unset()
    end

end
