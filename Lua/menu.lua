menu = {}
pause = {}

function menu.load()
    --draws the menu for the first time
    menuBG = love.graphics.newImage("images/menu.png")
    --menuBGM = love.audio.newSource("music/doodle-fit-menu.ogg")
    --menuBGM:setLooping(true)

    menuArrow = love.graphics.newImage("images/arrow.png")

    menuState = "start"

    menu.insertButton(650, 278.95, "start game", "start")
    menu.insertButton(650, 375.9, "quit", "quit")

    pauseBG = love.graphics.newImage("images/pause.png")
    pauseState = "resume"

    pause.insertButton(650, 278.95, "resume", "resume")
    pause.insertButton(650, 375.9, "quit", "quit1")

--   we no longer want options, but this is how you would do it
-- menu.insertButton(481, 375.9, "options", "options")
end

function menu.insertButton(x,y,text,id)
    --we are handling all buttons in a table
    --this is how you insert an item into the table
    --param: x, y (cordinates to be drawn) text (the text to be displayed)
    --id(object's name)
    table.insert(menu, {x = x, y = y, text = text, id = id})
end

function pause.insertButton(x,y,text,id)
    --similar to menu, but for the pause menu
    table.insert(pause, {x = x, y = y, text = text, id = id})
end

function menu.move(key)
    --handles moving between states of the game from the main menu
    --param: key that will handle which state we are targetting
    if key == 'down' and menuState == "start" then
        menuState = "quit"
    elseif key == 'up' and menuState == "quit" then
        menuState = "start"
    end

    love.graphics.clear()           -- SHOULD clear the screen
    menu.draw()                     -- Redraw the menu with the correct arrow in place
end

function pause.move(key)
    -- handles moving between states of the game from the pause menu
    -- param: key that will handle which state we go to now
    if key == 'down' and pauseState == "resume" then
        pauseState = "quit1"
    elseif key == 'up' and pauseState == "quit1" then
        pauseState = "resume"
    end

    love.graphics.clear()
    pause.draw()
end


function menu.pressButton(key)
    --in progress, will handle prospective movements in the game state
    --param: key that will enter the game playing state
    if key == 'enter' then
        -- do something according to respective game state
    end
end

function menu.draw()
    --handles the drawing of the menu
    --param: none
    love.graphics.draw(menuBG)
    love.graphics.setBackgroundColor(107, 114, 144)

    hsTxt = love.graphics.newFont("font/orange-juice-2.ttf", 45, 255, 0, 0)
    love.graphics.setFont(hsTxt)
    scoreString = "High Score: " .. highScore
    love.graphics.print(scoreString, 650, 200)

    love.graphics.setFont(menuOpsTxt)

    for i,v in ipairs(menu) do
        love.graphics.setColor(255, 255, 255)
        love.graphics.print(v.text,v.x,v.y)
    end

    if menuState == "start" then
        love.graphics.draw(menuArrow, 570, 300)
    end

    if menuState == "quit" then
        love.graphics.draw(menuArrow, 570, 400)
    end
end

function pause.draw()
    love.graphics.draw(pauseBG)
    love.graphics.setBackgroundColor(90, 40, 90)

    for i,v in ipairs(pause) do
        love.graphics.setColor(255, 255, 255)
        love.graphics.setFont(menuOpsTxt)
        love.graphics.print(v.text,v.x,v.y)
    end   

    if pauseState == "resume" then
        love.graphics.draw(menuArrow, 570, 300)
    end

    if pauseState == "quit1" then
        love.graphics.draw(menuArrow, 570, 400)
    end
end