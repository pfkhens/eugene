
splash = {}

function splash.load()
--loads the splash screen
    splash.x = 250
    splash.y = screenHeight/3
    countTimer = 25

end

function splash.update()
    while countTimer > 0 do
        countTimer = countTimer - 1
    end
end

function splash.draw()
    --draw the splash screen. 
    love.graphics.setFont(splashTxt)
    love.graphics.setBackgroundColor(0,0,0)
    love.graphics.setColor(255, 255, 0)
    love.graphics.print("3 shades of code \n   presents...", splash.x, splash.y)
end