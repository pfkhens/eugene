badbullet_speed = 900
badbullet = {}
  
local spawntimer = 0
local spawntimlim = 3

function badbullet.load()
end

function badbullet.spawn(x, y, dir)
    --inserts a badbullet into our table, very similar to zombie:spawn
    table.insert(badbullet, {width = 25, height = 25, x = x, y = y, dir = dir, speed = badbullet_speed})
end

function badbullet.draw()
    --inserts a badbullets into the table that holds all badbullets
    --param: the x and y position that should be inserted
    --and the direction it should be drawn
    for i,v in ipairs(badbullet) do
        love.graphics.setColor(0, 255, 255)
        if v.dir == 'right' then
           badbullet.pic = love.graphics.circle("fill", v.x, v.y, 25, 100)
        end
        if v.dir == 'left' then
           badbullet.pic = love.graphics.circle("fill", v.x, v.y, 25, 100)
        end
    end
end

function badbullet.update(dt)
    --move the positions of all the badbullets in their respective directions
    for i,v in ipairs(badbullet) do
       if v.dir == "right" then
            v.x = v.x + bullet_speed * dt
        end
        if v.dir == "left" then
            v.x = v.x - bullet_speed * dt
        end
    end
end

function badbullet.reset()
    --empties the badbullets tables
    for ia,va in ipairs(badbullet) do
        table.remove(badbullet, ia)
    end
end

function badbullet.shoot(dt)
    --this randomizes the creation of zombies
    --param: dt (time differential between draws)
    if yondaime.health > 0 then
        spawntimer = spawntimer + dt
        
        if spawntimer > spawntimlim then

            if yondaime.pic == yondaimeER then
            badbullet.spawn(yondaime.x + yondaime.width, yondaime.y + yondaime.height / 1.5, 'right')
            end

            if yondaime.pic == yondaimeEL then
            badbullet.spawn(yondaime.x - yondaime.width, yondaime.y + yondaime.height / 1.5, 'left')
            end

            spawntimelim = 3
            spawntimer = 0
        end
    end
end