
yondaime = {}

function yondaime.load()
    yondaimeEL = love.graphics.newImage("images/yondaime-l.png")
    yondaimeER = love.graphics.newImage("images/yondaime-r.png")

    yondaime.pic = yondaimeEL
    yondaime.speed = 200
    yondaime.x = 700
    yondaime.y = -100
    yondaime.health = 25

    yondaime.width = yondaime.pic:getWidth()
    yondaime.height = yondaime.pic:getHeight()
end


function yondaime.move(dt)
    dx = player.x - yondaime.x
    dy = (player.y - player.height) - yondaime.y
    yondaime.speed = yondaime.speed

    distance = math.sqrt(dx*dx + dy*dy)

    yondaime.x = yondaime.x + (dx / distance * yondaime.speed * dt)
    yondaime.y  = yondaime.y + (dy / distance * yondaime.speed * dt)

    if player.x > yondaime.x + yondaime.width then
        yondaime.pic = yondaimeER
    end
    
    if player.x < yondaime.x - yondaime.width then
        yondaime.pic = yondaimeEL
    end
end

function yondaime.update()
    if player.pic == playerER then
        yondaime.pic = yondaimeEL
    end

    if player.pic == playerEL then
        yondaime.pic = yondaimeER
    end

    if player.x + player.width > yondaime.x and
        player.x < yondaime.x + yondaime.width and
        player.y + player.height > yondaime.y and
        player.y < yondaime.y + yondaime.height then

        -- this is how we would kill the zombies if the run into us: table.remove(zombie, i)
        if yondaime.direction  == 'right' then
            yondaime.x = yondaime.x + 300
        else
            yondaime.x = yondaime.x - 300
        end
        player.decrementHeath()
    end
end

function yondaime.death()
    -- checks all of the yondaimes and all of the bullets to 
    -- see which collide, then remove all the bullets and yondaimes
    -- that have collided. Runtime O(n^2)
    for ia,va in ipairs(bullet) do
        if va.x + va.width > yondaime.x and
            --check to see if it intersects here
            va.x < yondaime.x + yondaime.width and
            va.y + va.height > yondaime.y and
            va.y < yondaime.y + yondaime.height then
            table.remove(bullet, ia)
                yondaime.health = yondaime.health - 1
            break
        end
    end
end

function yondaime.reset()
    for i,v in ipairs(yondaime) do
        table.remove(yondaime, i)
    end
    yondaime.x = 700
    yondaime.y = -100
    yondaime.health = 30    
end

function yondaime.draw()
    love.graphics.setColor(255,255,255)
    if yondaime.health > 0 then
        love.graphics.draw(yondaime.pic, yondaime.x, yondaime.y)
    end
end