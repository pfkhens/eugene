
function level1_load()
    tardisBG = love.graphics.newImage("images/bg/tardis2.png")
    levelOneBGM = love.audio.newSource("music/level1.ogg") -- load leve1 one BGM
    levelOneBGM:setLooping(true)

    groundlevel = 420
end

function level1_update()
end

function level1_draw()
    love.graphics.setBackgroundColor(0, 0, 0)
    love.graphics.draw(tardisBG)  -- display background
end