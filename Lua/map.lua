function map_collide()
    -- begins the player on each screen
    pic = love.graphics.newImage("images/eugene-r0.png")

    if player.x < 0 then
        player.x = 0
    end

    if currLevel == "tutLevel" then
        if player.x + player.pic:getWidth() > tutorialBG:getWidth() then
            player.x = tutorialBG:getWidth() - player.pic:getWidth()
        end

    elseif currLevel == "level1" then
        if player.x + player.pic:getWidth() > tardisBG:getWidth() then
            player.x = tardisBG:getWidth() - player.pic:getWidth()
        end

    elseif currLevel == "midBossLevel" or currLevel == "finalBossLevel" then
        if player.x + player.pic:getWidth() > 1300 then
            player.x = 1300 - player.pic:getWidth()
        end

    end
end