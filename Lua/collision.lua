
function loadAllCollisions()
	collider= hardCollide(100, on_collide, stop_collide)
end

function on_collide( dt, a, b, x, y )	
	-- captures which 2 objects are colliding with each other
	if(a.type == "player" and b.type =="zombie") or (b.type == "player" and a.type == "zombie") then
		player.decrementHeath()
		local player, zombie
		if(a.type == player) then
			player, zombie = a, b
		else
			zombie, player = a, b
		collider:remove(zombie)
		zombie.flag = false
		end
	end
	--repeat all for all the types
end

function stop_collide( dt, a, b )
	-- what to do when the objects stop colliding
end