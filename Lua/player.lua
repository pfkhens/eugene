
player = {}
playerER = {}
playerEL = {}

playerHealth = {}

function player.load()
    player.x = 5
    player.y = 150

    player.xVelocity = 0
    player.yVelocity = 0

    player.friction = 5
    player.speed = 3000
    player.ySpeed = 0 
    player.gravSecond = 3  
    inAir = false

    --player moving right images
    playerER[1] = love.graphics.newImage("images/eugene-r0.png")
    playerER[2] = love.graphics.newImage("images/eugene-r1.png")
    playerER[3] = love.graphics.newImage("images/eugene-r2.png")
    playerER[4] = love.graphics.newImage("images/eugene-r3.png")
    playerER[5] = love.graphics.newImage("images/eugene-jump-r0.png")
   
   --player moving left images 
    playerEL[1] = love.graphics.newImage("images/eugene-l0.png")
    playerEL[2] = love.graphics.newImage("images/eugene-l1.png")
    playerEL[3] = love.graphics.newImage("images/eugene-l2.png")
    playerEL[4] = love.graphics.newImage("images/eugene-l3.png")
    playerEL[5] = love.graphics.newImage("images/eugene-jump-l0.png")
    
    --player health images
    playerHealth[1] = love.graphics.newImage("images/hearts1.png")
    playerHealth[2] = love.graphics.newImage("images/hearts2.png")
    playerHealth[3] = love.graphics.newImage("images/hearts3.png")
    playerHealth[4] = love.graphics.newImage("images/hearts4.png")
    playerHealth[5] = love.graphics.newImage("images/hearts5.png")
    playerHealth[6] = love.graphics.newImage("images/hearts6.png")

        player.animtimer = 0
    player.pic = playerER[1]
    player.width = player.pic:getWidth()
    player.height = player.pic:getHeight()
    player.pnum = 1
    player.health = 12
    player.healthCtr = 1
    gravity = 400
    mjump_height = 300
end


function player.decrementHeath()
    --this decrements the player's health
    --it does NOT handle deleting the player if it dies
    player.health = player.health - 1
    -- if (collision happens) then
    -- Having it remove the health twice actually reduces the amount of damage to just one heart each time
    player.health = player.health - 1
    player.healthCtr = player.healthCtr + 1

    if score ~= 0 then
        score = score - 925
    end
end

function player.update(dt)
    player.physics(dt)
    
    if love.keyboard.isDown('right') and player.xVelocity < player.speed then
        player.xVelocity = player.xVelocity + player.speed * dt
        player.pic = playerER[player.pnum]
        player.animtimer = player.animtimer + dt

        if player.animtimer > 0.1 then
            player.pnum = player.pnum + 1
            player.animtimer = 0
        end

        if player.pnum > 4 then
            player.pnum = 1
        end
    end

    if love.keyboard.isDown('left') and player.xVelocity > -player.speed then
        player.xVelocity = player.xVelocity - player.speed * dt
        player.pic = playerEL[player.pnum]
        player.animtimer = player.animtimer + dt

        if player.animtimer > 0.1 then
            player.pnum = player.pnum + 1
            player.animtimer = 0
        end

        if player.pnum > 4 then
            player.pnum = 1
        end
    end
    -- I do this just because I think better this way
    frame = dt * 30  
    -- We add gravity to the ySpeed
    player.ySpeed = player.ySpeed + player.gravSecond * frame 
    -- We add ySpeed to the player's y position
    player.y = player.y + player.ySpeed * frame 
    -- Are we on the ground?
    if player.y > 200 then 
        -- If we are, We undo that moving down
        player.y = player.y - player.ySpeed * frame 
        -- The ySpeed is reset
        player.ySpeed = 0 
        player.inAir = false
    end
end

function player.animreset(key)
    --this is our keyhandling method
    --param: the key that we should be handling
    if key == "right" then
        player.animtimer = 0
        player.pnum = 1
        player.pic = playerER[1]
    end

    if key == "left" then
        player.animtimer = 0
        player.pnum = 1
        player.pic = playerEL[1]
    end
    -- Are we on the ground?
    if key == " " and player.y < 200 and not player.inAir   then 
        -- Make us add a negative, to move up
        player.ySpeed = -40
        player.inAir = true
    end
end

function player.draw()
    love.graphics.setColor(255,255,255)
    love.graphics.draw(player.pic, player.x, player.y)
end

function player.reset()
    player.health = 12
    player.healthCtr = 1
    player.x = 1360
    player.y = 180

    player.animtimer = 0
    player.pic = playerER[1]
    player.pnum = 1
    kills = 0    
end

function player.drawHealth(player)
    love.graphics.draw(playerHealth[player.healthCtr], camera:getX() + 25, camera:getY() + 25)
end

function player.drawkills()
    love.graphics.setColor(0,255,255)
    love.graphics.print("Score: ", camera:getX() + 700, camera:getY() + 20)--capital s looks good here
    love.graphics.print(score, camera:getX() + 950, camera:getY() + 20)   
end    


function player.physics(dt)
    player.x = player.x + player.xVelocity * dt
    player.y = player.y + player.yVelocity * dt
    player.xVelocity = player.xVelocity * (1 - math.min(dt*player.friction, 1))
end

function player.move(dt)
    if love.keyboard.isDown('right') and player.xVelocity < player.speed then
        player.xVelocity = player.xVelocity + player.speed * dt
    end

    if love.keyboard.isDown('left') and player.xVelocity > -player.speed then
        player.xVelocity = player.xVelocity - player.speed * dt
    end
end

function player.boundary()
    if player.x < 0 then
        player.x = 0
        player.xVelocity = 0
    end
    if player.y + player.height > groundlevel then
        player.y = groundlevel - player.height
        player.yVelocity = 0
    end
end

function player:collide()
    --checks our player against all zombies, and collides with the necessary zombies
    --when we collide, we push the zombie in the appropriate direction, and decrement the player's health
    --param: none
    for i,v in ipairs(zombie) do
        if player.x + player.width > v.x and
            player.x < v.x + v.width and
            player.y + player.height > v.y and
            player.y < v.y + v.height then
            -- this is how we would kill the zombies if the run into us: table.remove(zombie, i)
            if v.direction  == 'right' then
                v.x = v.x + 300
            else
                v.x = v.x - 300
            end
            player.decrementHeath()

            break
        end
    end

    for i, v in ipairs(badbullet) do
        if player.x + player.width > v.x and
            player.x < v.x + v.width and
            player.y + player.height > v.y and
            player.y < v.y + v.height then

            table.remove(badbullet, i)
            player.decrementHeath()
            player.decrementHeath()
        end
    end
end

function updateEugene(dt)
    player.physics(dt)
    player:collide()
    player.update(dt)
end

function drawEugene()
    if currLevel ~= "tutLevel" then 
        player.draw()
        player.drawHealth(player)
        player.drawkills()
    else
        player.draw()
    end
end