bullet_speed = 900
bullet = {}

bulletImg = love.graphics.newImage("images/bullet-1.png")
bulletImgL = love.graphics.newImage("images/bullet-2.png")

bullet.pic = bulletImg
blaster = love.audio.newSource("sounds/laser.wav") --load sound

function bullet.load()
end

function bullet.spawn(x, y, dir)
    --inserts a bullet into our table, very similar to zombie:spawn
    table.insert(bullet, {width = 10, height = 10, x = x, y = y, dir = dir})
end

function bullet.draw()
    --inserts a bullets into the table that holds all bullets
    --param: the x and y position that should be inserted
    --and the direction it should be drawn
    for i,v in ipairs(bullet) do
        love.graphics.setColor(255, 255, 255)
        if v.dir == 'right' then
            love.graphics.draw(bullet.pic, v.x, v.y)
        end

        if v.dir == 'left' then
            love.graphics.draw(bulletImgL, v.x, v.y)
        end
    end
end

function bullet.update(dt)
    --move the positions of all the bullets in their respective directions
    for i,v in ipairs(bullet) do
        if v.dir == "right" then
            v.x = v.x + bullet_speed * dt
        end

        if v.dir == "left" then
            v.x = v.x - bullet_speed * dt
        end
    end
end

function bullet.reset()
    --empties the bullets tables
    for ia,va in ipairs(bullet) do
        table.remove(bullet, ia)
    end
end

function bullet.shoot(key)
    --notes when we are wanting to shoot a bullet, and spawns a bullet
    --sound issues with blaster
    if key == 'a' and player.pic == playerER[player.pnum] then
        bullet.spawn(player.x + player.width, player.y + player.height / 1.5, 'right')
         -- blaster:stop()
         -- blaster:play()
    end
    if key == 'a' and player.pic == playerEL[player.pnum] and gamestate == "playing" then
        bullet.spawn(player.x - player.width, player.y + player.height / 1.5, 'left')
         -- blaster:stop()
         -- blaster:play()
    end
    
end